package pl.grudzienm.ktozbiera.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by mateusz on 27.11.2016.
 */
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date date;
    private String person;
    private String dish;
    private String place;
    private Double price;

    public Order(){}

    public Order(Date date, String person, String dish, String place, String price) {
        this.date = date;
        this.person = person;
        this.dish = dish;
        this.place = place;
        this.price = parsePrice(price);
    }

    public Long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    private Double parsePrice(String price) {
        if (price.contains(",")) {
            price = price.replace(',', '.');
        }

        return Double.parseDouble(price);
    }
}
