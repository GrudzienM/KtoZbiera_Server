package pl.grudzienm.ktozbiera.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by mateusz on 27.11.2016.
 */
@Entity
@Table(name = "days_since_ordering")
public class DaysSinceOrdering {

    @Id
    private String person;
    private Integer days;

    public DaysSinceOrdering() {
    }

    public DaysSinceOrdering(String person, Integer days) {
        this.person = person;
        this.days = days;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }
}
