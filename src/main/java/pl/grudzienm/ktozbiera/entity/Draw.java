package pl.grudzienm.ktozbiera.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by mateusz on 10.12.2016.
 */
@Entity
@Table(name = "draw")
public class Draw {

    @Id
    private Date date;
    private String winner;

    public Draw() {
    }

    public Draw(Date date, String winner) {
        this.date = date;
        this.winner = winner;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }
}
