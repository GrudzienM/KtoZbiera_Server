package pl.grudzienm.ktozbiera.utils.query;

import javax.persistence.Query;
import java.util.Calendar;

/**
 * Created by mateusz on 08.12.2016.
 */
public class QueryParametrizationUtils {

    public static void setDateRange(Query query, QueryDateRange dateRange) {
        switch (dateRange) {
            case PRESENT_DAY:
                setTodaysDateRange(query);
                break;
        }
    }

    private static void setTodaysDateRange(Query query) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        query.setParameter("startDate", cal.getTime());
        cal.add(Calendar.DATE, 1);
        query.setParameter("endDate", cal.getTime());
    }
}
