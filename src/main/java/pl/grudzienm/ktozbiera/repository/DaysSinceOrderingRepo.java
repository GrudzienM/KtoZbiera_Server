package pl.grudzienm.ktozbiera.repository;

import org.springframework.stereotype.Repository;
import pl.grudzienm.ktozbiera.entity.DaysSinceOrdering;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mateusz on 27.11.2016.
 */
@Repository
@Transactional
public class DaysSinceOrderingRepo {

    @PersistenceContext
    EntityManager entityManager;

    private final String QUERY_SELECT_DAYSSINCEORDERING = "SELECT dso FROM DaysSinceOrdering dso";

    public void incrementDays(String person) {
        DaysSinceOrdering daysSinceOrdering;
        try {
            daysSinceOrdering = (DaysSinceOrdering) entityManager.createQuery(QUERY_SELECT_DAYSSINCEORDERING + " WHERE dso.person = :person").setParameter("person", person).getSingleResult();
            daysSinceOrdering.setDays(daysSinceOrdering.getDays() + 1);
        } catch (NoResultException e) {
            daysSinceOrdering = new DaysSinceOrdering(person, 1);
        }

        entityManager.persist(daysSinceOrdering);
    }

    public void resetDaysCounter(String person) {
        Query query = entityManager.createQuery("UPDATE DaysSinceOrdering dso SET dso.days = 0 WHERE dso.person = :person");
        query.setParameter("person", person);
        query.executeUpdate();
    }

    public Integer fetchDays(String person) {
        Integer result;
        DaysSinceOrdering daysSinceOrdering;
        try {
            daysSinceOrdering = (DaysSinceOrdering) entityManager.createQuery(QUERY_SELECT_DAYSSINCEORDERING + " WHERE dso.person = :person").setParameter("person", person).getSingleResult();
            result = daysSinceOrdering.getDays();
        } catch (NoResultException e) {
            result = 0;
        }

        return result;
    }

    public Map<String, Integer> fetchDaysForUsers(List<String> users) {
        HashMap<String, Integer> result = new HashMap<>();

        for (String user : users) {
            Integer days = fetchDays(user);
            result.put(user, days);
        }

        return result;
    }

    public List<String> fetchUsersWithHighestDaysCount() {
        return entityManager.createQuery("SELECT dso.person FROM DaysSinceOrdering dso WHERE dso.days = (" +
                    "SELECT MAX(dso.days) FROM DaysSinceOrdering dso)").getResultList();

    }

    /*public List<DaysSinceOrdering> fetchDaysSinceOrderingAndOrderByHighestCount() {
        List<DaysSinceOrdering> daysSinceOrdering = entityManager.createQuery(QUERY_SELECT_DAYSSINCEORDERING + " ORDER BY dso.days").getResultList();

        return daysSinceOrdering;
    }*/
}
