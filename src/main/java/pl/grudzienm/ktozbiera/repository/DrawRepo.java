package pl.grudzienm.ktozbiera.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.grudzienm.ktozbiera.entity.Draw;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by mateusz on 10.12.2016.
 */
@Repository
@Transactional
public class DrawRepo {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private DaysSinceOrderingRepo daysSinceOrderingRepo;

    public String conductDraw(boolean updateTables) {
        String winner = drawWinner();
        if (updateTables && winner != null && !winner.equals("")) {
            daysSinceOrderingRepo.resetDaysCounter(winner);
            updateTableWithTodaysWinner(winner);
        }

        return winner;
    }

    public boolean wasDrawConducted() {
        Query query = entityManager.createQuery("SELECT draw.winner FROM Draw draw WHERE date = :date");
        query.setParameter("date", new Date());
        try {
            query.getSingleResult();
        } catch (NoResultException e) {
            return false;
        }

        return true;
    }

    public Draw getTodaysDraw() {
        Draw draw;
        Query query = entityManager.createQuery("SELECT draw FROM Draw draw WHERE date = :date");
        query.setParameter("date", new Date());
        try {
            draw = (Draw) query.getSingleResult();
        } catch (NoResultException e) {
            draw = new Draw();
        }

        return draw;
    }

    private String drawWinner() {
        List<String> peopleThatOrderedToday = orderRepo.getPeopleThatOrderedToday();
        List<String> peopleWithHighestDaysCount = daysSinceOrderingRepo.fetchUsersWithHighestDaysCount();
        peopleThatOrderedToday.retainAll(peopleWithHighestDaysCount);
        Collections.shuffle(peopleThatOrderedToday);

        if (peopleThatOrderedToday.size() > 0) {
            return peopleThatOrderedToday.get(0);
        } else {
            return "";
        }
    }

    private void updateTableWithTodaysWinner(String luckyPerson) {
        Draw draw = new Draw(new Date(), luckyPerson);
        entityManager.persist(draw);
    }
}
