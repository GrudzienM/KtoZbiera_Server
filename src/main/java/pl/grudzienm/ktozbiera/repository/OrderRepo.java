package pl.grudzienm.ktozbiera.repository;

import org.springframework.stereotype.Repository;
import pl.grudzienm.ktozbiera.entity.Order;
import pl.grudzienm.ktozbiera.utils.query.QueryDateRange;
import pl.grudzienm.ktozbiera.utils.query.QueryParametrizationUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mateusz on 27.11.2016.
 */
@Repository
@Transactional
public class OrderRepo {

    @PersistenceContext
    private EntityManager entityManager;

    public void placeOrder(String user, String dish, String place, String price) {
        Order order = new Order(new Date(), user, dish, place, price);
        entityManager.persist(order);
    }

    public List<Order> getTodaysOrders() {
        Query query = entityManager.createQuery("SELECT o FROM Order o WHERE o.date BETWEEN :startDate AND :endDate");
        QueryParametrizationUtils.setDateRange(query, QueryDateRange.PRESENT_DAY);
        List<Order> todaysOrders = query.getResultList();

        return todaysOrders;
    }

    public List<String> getPeopleThatOrderedToday() {
        List<String> people = new ArrayList<String>();
        Query query = entityManager.createQuery("SELECT o.person FROM Order o WHERE o.date BETWEEN :startDate AND :endDate");
        QueryParametrizationUtils.setDateRange(query, QueryDateRange.PRESENT_DAY);
        people = (List<String>) query.getResultList().stream().distinct().collect(Collectors.toList());

        return people;
    }
}
