package pl.grudzienm.ktozbiera.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grudzienm.ktozbiera.entity.Order;
import pl.grudzienm.ktozbiera.repository.DaysSinceOrderingRepo;
import pl.grudzienm.ktozbiera.repository.OrderRepo;

import java.util.List;

/**
 * Created by mateusz on 27.11.2016.
 */
@RestController
@RequestMapping("orders")
public class OrderController {

    @Autowired
    OrderRepo orderRepo;

    @Autowired
    DaysSinceOrderingRepo daysSinceOrderingRepo;

    @RequestMapping(value = "place", method = RequestMethod.POST)
    public String placeOrder(@RequestParam String user, @RequestParam String dish, @RequestParam String place, @RequestParam String price) {
        orderRepo.placeOrder(user, dish, place, price);

        daysSinceOrderingRepo.incrementDays(user);

        return "success";
    }

    @RequestMapping("today")
    public List<Order> fetchTodaysOrders() {
        return orderRepo.getTodaysOrders();
    }
}
