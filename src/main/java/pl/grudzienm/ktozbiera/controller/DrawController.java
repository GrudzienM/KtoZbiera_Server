package pl.grudzienm.ktozbiera.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.grudzienm.ktozbiera.entity.DaysSinceOrdering;
import pl.grudzienm.ktozbiera.entity.Draw;
import pl.grudzienm.ktozbiera.repository.DaysSinceOrderingRepo;
import pl.grudzienm.ktozbiera.repository.DrawRepo;
import pl.grudzienm.ktozbiera.repository.OrderRepo;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

/**
 * Created by mateusz on 27.11.2016.
 */
@RestController
@RequestMapping("draw")
public class DrawController {

    @Autowired
    private DrawRepo drawRepo;

    @RequestMapping(value = "conduct", method = RequestMethod.POST)
    public String conductDraw() {
        String winner;

        if (drawRepo.wasDrawConducted()) {
            winner = drawRepo.conductDraw(false);
        } else {
            winner = drawRepo.conductDraw(true);
        }

        return winner;
    }

    @RequestMapping("today")
    public Draw getTodays() {
        return drawRepo.getTodaysDraw();
    }
}
