CREATE TABLE public.orders
(
    id integer NOT NULL DEFAULT nextval('orders_id_seq'::regclass),
    person character varying COLLATE pg_catalog."default" NOT NULL,
    dish character varying(300) COLLATE pg_catalog."default" NOT NULL,
    place character varying(200) COLLATE pg_catalog."default",
    price numeric,
    date date,
    CONSTRAINT orders_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.orders
    OWNER to postgres;




CREATE TABLE public.days_since_ordering
(
    person character varying COLLATE pg_catalog."default",
    days numeric
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.days_since_ordering
    OWNER to postgres;




CREATE TABLE public.draw
(
    date date,
    winner character varying
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.draw
    OWNER to postgres;